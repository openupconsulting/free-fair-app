var unsentDataHandler = {
    values: [], 
    
    send: function(values) {
        var deferred = $.Deferred();
        this.values = values;
        this.sendInner(deferred);
        return deferred.promise();
    },
    
    sendInner: function(deferred) {
        if (this.values.length == 0) {
            deferred.resolve();
        }
        
        var unsent = this.values[0];
        if (unsent == undefined) {
            deferred.resolve();
        }
        
        googleapi.getToken(config.authOptions).done(function(token) {
            var promise = remoteData.saveResults(token.access_token, unsent.url, unsent.data);            
            $.when(promise).then(function() {
                var deletePromise = localDataDeleteUnsentResults(unsent.rowid);
                $.when(deletePromise).then(function() {
                    unsentDataHandler.values.shift();
                    unsentDataHandler.sendInner(deferred);
                }, deferred.reject);
            }, deferred.reject);        
        }).fail(deferred.reject);
    }
};
