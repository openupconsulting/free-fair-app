/**************************************************************
*This file is part of Free & Fair, an electoral transparency mobile app.
*  Copyright (C) 2014 The Free & Fair Team
* 
*  Free & Fair is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  
*  Free & Fair is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************/
$(document).on('deviceready', function() {
    
    $(document).on("pageshow", "#page-splash", function(event) { 
        $("#electionsapp").css('visibility', 'visible');
        initData();
       
        navigator.geolocation.getCurrentPosition(function(position) {
            reportData.longitude = position.coords.longitude;
            reportData.latitude = position.coords.latitude;
        });
    });

    function initData() { 
	   console.log('init_key: ' + localStorage.activeInitiativeKey);
	   console.log('init_loaded: ' + localStorage.initiatives_loaded);
	   	if (localStorage.initiatives_loaded === "loaded") { 
            initLoaded();
            return;
        }
	
		
        $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
    
        googleapi.getToken(config.authOptions).done(function(data) {
			
			ongoingInitData.loadData(data.access_token, initLoaded, dataLoadFailed);
			
        }).fail(function(data) {
            dataLoadFailed();
        });
    } 

    function initLoaded() {
		 if (localStorage.initiatives_loaded != "loaded") {
		
		 ongoingInitData.loadData(localStorage.access_token, function()
		  {  // var promise = localDataLoadAppTexts();
		         var promise = localDataLoadInitiatives();
				  $.when(promise).then(function(data) {
                $.mobile.changePage("#page-initiatives", { transition: "none" });
				       
        });
		   },  function() {});
			 }
			 
			 else { 
			    var promise = localDataLoadInitiatives();
				  $.when(promise).then(function(data) {
                $.mobile.changePage("#page-initiatives", { transition: "none" });
			 });
			 
			 }
      
        
       
        
       
    }
    
    function dataLoadFailed() {
		alert('data load failed');
        initInProgress = false;
        
        $.mobile.loading("hide");
        $("#splash-data-load-error").popup("open", { transition: "none", position: "window" });
    }	
    
});


