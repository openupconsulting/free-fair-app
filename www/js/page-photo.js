$(document).on("pageinit", "#page-photo", function(event) { 	
   
    $('#photo-capture-done').bind('mouseup', function(e) {
        $.mobile.changePage("#page-task", { transition: "none" });        
    });

    $.when(localDataLoadAppText("email results")).then(function(text) {
        $('#send-photo-via-email').attr('href', "mailto:" +  localStorage.activeFormCode + "?subject=" + encodeURIComponent(localStorage.activeFormName) + " " 
                                       + encodeURIComponent(reportData.votingSite.id + ' ' + reportData.votingSite.name));
    });
    $('#photo-init-title').text(localStorage.activeFormName);
    $('#take-photo').bind('mouseup', function(e) {
        navigator.camera.getPicture(onSuccess, onFail, { 
            quality: 50,
            destinationType: Camera.DestinationType.FILE_URL,
            saveToPhotoAlbum: true
        }); 
    });
        
    function onSuccess(imageData) {
    }
    
    function onFail(message) {
    }	
});

$(document).on("pageshow", "#page-photo", function(event) { 

    $("#page-photo-title").text(reportData.votingSite.id + ' ' + reportData.votingSite.name);
    
    navigator.geolocation.getCurrentPosition(function(position) {
        reportData.longitude = position.coords.longitude;
        reportData.latitude = position.coords.latitude;
    });

});
