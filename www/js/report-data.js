var reportData = {
    votingSite: {},
	activeInitiativeKey: 0, 
    longitude: 0,
    latitude: 0,
	  
    prepareNoonParticipationResults: function(votes, comment) {
        var entry = [];
		var element = {};
		  element['PlaceID'] = reportData.votingSite.id 
		  entry.push(element); 
		  var element = {};
          element['Email'] =   localStorage.user_email 
		  entry.push(element);
		   var element = {};
          element['Latitude'] = reportData.latitude 
		  entry.push(element);
		  var element = {};
          element['Longitude'] = reportData.longitude 
		  entry.push(element);
		  var element = {};
          element['Time'] = "00:00"    
		  entry.push(element);
		   var element = {};
          element['Nparticipation'] = votes 
		  entry.push(element);
		   var element = {};
		  element['initkey'] = localStorage.activeInitiativeKey
		  entry.push(element); 
		    var element = {};
		  element['Comments'] = encodeURIComponent(comment);
		  entry.push(element); 
      return entry;
    },

    prepareClosingParticipationResults: function(registered, voted, valid, invalid) {
        var entry = '<entry xmlns="http://www.w3.org/2005/Atom" ' 
            + ' xmlns:gsx="http://schemas.google.com/spreadsheets/2006/extended">'
            + '<gsx:siteid>' + reportData.votingSite.id + '</gsx:siteid>'
            + '<gsx:userid>' + localStorage.user_email + '</gsx:userid>'
            + '<gsx:latitude>' + reportData.latitude + '</gsx:latitude>'
            + '<gsx:longitude>' + reportData.longitude + '</gsx:longitude>'
            + '<gsx:timerecorded>' + this.formatDate(new Date()) + '</gsx:timerecorded>'
            + '<gsx:timesent>##TIMESTAMP##</gsx:timesent>'
            + '<gsx:registered>' + registered + '</gsx:registered>'
            + '<gsx:voted>' + voted + '</gsx:voted>'
            + '<gsx:validballots>' + valid + '</gsx:validballots>'
            + '<gsx:invalidballots>' + invalid + '</gsx:invalidballots>'
            + '</entry>';    
        return entry;
    },

    prepareFinalResults: function(votes) {
        var results = '';
        
        $.each(votes, function(name, value) {
            results = results + '<gsx:' + name + '>' + value + '</gsx:' + name + '>';
        });
        
        var entry = '<entry xmlns="http://www.w3.org/2005/Atom" ' 
            + ' xmlns:gsx="http://schemas.google.com/spreadsheets/2006/extended">'
            + '<gsx:siteid>' + reportData.votingSite.id + '</gsx:siteid>'
            + '<gsx:userid>' + localStorage.user_email + '</gsx:userid>'
            + '<gsx:latitude>' + reportData.latitude + '</gsx:latitude>'
            + '<gsx:longitude>' + reportData.longitude + '</gsx:longitude>'
            + '<gsx:timerecorded>' + this.formatDate(new Date()) + '</gsx:timerecorded>'
            + '<gsx:timesent>##TIMESTAMP##</gsx:timesent>'
            + results
            + '</entry>';
        
        return entry;
    },
    
    formatDate: function(date) {
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
    
        var hour = date.getHours();
        var minute = date.getMinutes();
        var second = date.getSeconds();
        
        return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
    }
}

