var unsentDataHandler = {
    values: [], 
    
    send: function(values) {
        var deferred = $.Deferred();
        this.values = values;
        this.sendInner(deferred);
        return deferred.promise();
    },
    
    sendInner: function(deferred) {
        if (this.values.length == 0) {
            deferred.resolve();
        }
        
        var unsent = this.values[0];
        if (unsent == undefined) {
            deferred.resolve();
        }
        
        googleapi.getToken(config.authOptions).done(function(token) {
            var promise = remoteData.saveUnsentResults(token.access_token, unsent.data);            
            $.when(promise).then(function() {
                var deletePromise = localDataDeleteUnsentResults(unsent.rowid);
                $.when(deletePromise).then(function() {
                    unsentDataHandler.values.shift();
                    unsentDataHandler.sendInner(deferred);
                }, deferred.reject ); // deletepromise  was  
            }, deferred.reject );  // promise savereults    was deferred.reject    
        }).fail(unsentDataHandler.sendSms(deferred, unsent.smsno, unsent.msg, unsent.rowid)); // fail gettoken  
    },  // treba da testiram da li radi ovako. i da izbrisem send failed u page-form kod slanja smsa
	
	sendSms: function(deferred, smsno, msg, rowid) {
		
		  if( msg.length <  160 ) {  
            var intent = ""; //leave empty for sending sms using default intent
            var success =  function () { 
			   var deletePromise = localDataDeleteUnsentResults(rowid);
                $.when(deletePromise).then(function() {
                    unsentDataHandler.values.shift();
                    unsentDataHandler.sendInner(deferred);
                },  deferred.reject )
			   };
			var failure =   function (e) {   deferred.reject   };
		 	sms.send( '+'+smsno, msg, intent, success , failure );  
		}
			else { 
		     alert( "Poruka nije poslata jer ima više od 160 karaktera. molimo pozovite nas i saopštite rezultate " );
		   }  
	
} 

 
			   
			   }
			   
			   
			   
			   
			   
			    
			   


			   
			   
			   