/**************************************************************
*This file is part of Free & Fair, an electoral transparency mobile app.
*  Copyright (C) 2014 The Free & Fair Team
* 
*  Free & Fair is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  
*  Free & Fair is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************/
$(document).on("pageinit", "#page-photo", function(event) { 	
   
    $('#photo-capture-done').bind('mouseup', function(e) {
        $.mobile.changePage("#page-task", { transition: "none" });        
    });

    $.when(localDataLoadAppText("Picture reminder")).then(function(text) {
        $('#send-photo-via-email').attr('href', "mailto:" +  localStorage.activeFormCode + "?subject=" + encodeURIComponent(localStorage.activeFormName) + " " 
                                       + encodeURIComponent(reportData.votingSite.id + ' ' + reportData.votingSite.name)+'&body=' + text);
    });
    $('#photo-init-title').text(localStorage.activeFormName);
    $('#take-photo').bind('mouseup', function(e) {
        navigator.camera.getPicture(onSuccess, onFail, { 
            quality: 50,
            destinationType: Camera.DestinationType.FILE_URL,
            saveToPhotoAlbum: true
        }); 
    });
        
    function onSuccess(imageData) {
    }
    
    function onFail(message) {
    }	
});

$(document).on("pageshow", "#page-photo", function(event) { 

    $("#page-photo-title").text(reportData.votingSite.id + ' ' + reportData.votingSite.name);
    
    navigator.geolocation.getCurrentPosition(function(position) {
        reportData.longitude = position.coords.longitude;
        reportData.latitude = position.coords.latitude;
    });

});
