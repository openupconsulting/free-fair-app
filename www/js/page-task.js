$(document).on("pageinit", "#page-task", function(event) { 	
   
     function initData() {
				
        var items = "";
        
        $.when(localDataLoadTasks()).then(function (tasks) {

            $.each(tasks, function (index, task) {	 	 				
        items += '<li data-name="' + task.menuentry + '" data-type="'+task.type+'"'  + ' data-code="'+task.data+'"><a href="#"> ' + task.menuentry + '</a></li>'; 
            });
            
            $("#task-list").html(items);
            $("#task-list").listview("refresh");  
			$('#task-list').children('li').bind('mouseup', function(e) {
		
		localStorage.activeFormName =    $(this).attr('data-name');
	
		if( $(this).attr('data-type') == "FORM")	{ 
	//	 console.log('changing page to '+$(this).attr('data-type'));
		 $.mobile.changePage("#page-form", { transition: "none" });
		 $("#page-form").trigger('refresh')
	//	  console.log('should have changed page to '+$(this).attr('data-type'));
		  }
		else { 
	//	  console.log('changing page to '+$(this).attr('data-type'));
		  localStorage.activeFormName =    $(this).attr('data-name');
		  localStorage.activeFormCode =    $(this).attr('data-code');
		  $.mobile.changePage("#page-photo", { transition: "none" }); }
	});           
        });	
    }

    initData(); //first run
   
   
		 
	
    $('#submit-unsent-from-task').bind('mouseup', function(e) {
        $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
        
        var promise = localDataLoadUnsentResults();
        
        $.when(promise).then(function(unsent) {
            var sendPromise = unsentDataHandler.send(unsent);
            $.when(sendPromise).then(dataSent, dataSendFailed);
        });
		
    });

    function dataSent() {
        $.mobile.loading("hide");
        $("#div-submit-unsent-from-task").hide();            
    }
    
    function dataSendFailed() {
        $.mobile.loading("hide");
        $("#task-data-send-error").popup("open", { transition: "none", position: "window" });        
    }
});

$(document).on("pageshow", "#page-task", function(event) { 
	  remoteData.checkConnection();
	 if(localStorage.networkState == 'No') {
    	
    	$("#div-refresh-ptasks").hide();
    	}    
    	else { $("#div-refresh-ptasks").show(); }
        
        
    $("#page-task-title").text(reportData.votingSite.id + ' ' + reportData.votingSite.name);

    var promise = localDataLoadUnsentResults();
    
    $.when(promise).then(function(unsentResults) {
        if (unsentResults.length > 0) {
            $("#div-submit-unsent-from-task").show();            
            unsentTask = unsentResults;
        } else {
            $("#div-submit-unsent-from-task").hide();            
        }
    });
    
    navigator.geolocation.getCurrentPosition(function(position) {
        reportData.longitude = position.coords.longitude;
        reportData.latitude = position.coords.latitude;
    }); 
});


 $('#tasks-refresh').bind('mouseup', function (e) {
        $("#voting-site-list").html("");

        $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
    
        googleapi.getToken(config.authOptions).done(function (data) {
            remoteData.loadData(data.access_token, function () {

                    localizeApp(); //   dataLoaded(); was here          
                    initData();
                    $.mobile.loading("hide");    
            }, dataLoadFailed);
        }).fail(function (data) {
            dataLoadFailed();
        });
    });