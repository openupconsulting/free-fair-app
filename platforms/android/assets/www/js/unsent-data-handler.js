/**************************************************************
*This file is part of Free & Fair, an electoral transparency mobile app.
*  Copyright (C) 2014 The Free & Fair Team
* 
*  Free & Fair is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  
*  Free & Fair is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************/

var unsentDataHandler = {
    values: [], 
    
    send: function(values) {
        var deferred = $.Deferred();
        this.values = values;
        this.sendInner(deferred);
        return deferred.promise();
    },
    
    sendInner: function(deferred) {
        if (this.values.length == 0) {
            deferred.resolve();
        }
        
        var unsent = this.values[0];
        if (unsent == undefined) {
            deferred.resolve();
        }
        

             googleapi.getToken(config.authOptions).done(function(token) {
            var promise = remoteData.saveUnsentResults(token.access_token, unsent.data);            
            $.when(promise).then(function() {
                var deletePromise = localDataDeleteUnsentResults(unsent.rowid);
                $.when(deletePromise).then(function() {
                    unsentDataHandler.values.shift();
                    unsentDataHandler.sendInner(deferred);
                }, deferred.reject);
            }, deferred.reject);        
        }).fail(deferred.reject);



 /*       googleapi.getToken(config.authOptions).done(function(token) {
            var promise = remoteData.saveUnsentResults(token.access_token, unsent.data);            
            $.when(promise).then(function() {
                var deletePromise = localDataDeleteUnsentResults(unsent.rowid);
                $.when(deletePromise).then(function() {
                    unsentDataHandler.values.shift();
                    unsentDataHandler.sendInner(deferred);
                }, deferred.reject ); // deletepromise  was  
            }, deferred.reject );  // promise savereults    was deferred.reject    
        }).fail(unsentDataHandler.sendSms(deferred, unsent.smsno, unsent.msg, unsent.rowid)); // fail gettoken  */
    },  // treba da testiram da li radi ovako. i da izbrisem send failed u page-form kod slanja smsa
	
	sendSms: function(deferred, smsno, msg, rowid) {
		
		  if( msg.length <  160 ) {  
            var intent = ""; //leave empty for sending sms using default intent
            var success =  function () { 
			   var deletePromise = localDataDeleteUnsentResults(rowid);
                $.when(deletePromise).then(function() {
                    unsentDataHandler.values.shift();
                    unsentDataHandler.sendInner(deferred);
                },  deferred.reject )
			   };
			var failure =   function (e) {   deferred.reject   };
		 	sms.send( '+'+smsno, msg, intent, success , failure );  
		}
			else { 
		     alert( "Poruka nije poslata jer ima više od 160 karaktera. molimo pozovite nas i saopštite rezultate " );
		   }  
	
} 

 
			   
			   }
			   
			   
			   
			   
			   
			    
			   


			   
			   
			   