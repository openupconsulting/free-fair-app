/**************************************************************
*This file is part of Free & Fair, an electoral transparency mobile app.
*  Copyright (C) 2014 The Free & Fair Team
* 
*  Free & Fair is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  
*  Free & Fair is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************/

$(document).on("pageinit", "#page-legal", function(event) { 	
	
    $.when(localDataLoadAppText("Warning text")).then(function(text) {
        $("#legalText").text(text);
    });
     $.when(localDataLoadAppText("Accept and continue")).then(function(text) {
        $("#legal-accept").text(text);
    });
	
	$("#legal-accept").click(function(event) {
        localStorage.legal_accepted = "accepted";
		 $.mobile.changePage("#page-site", { transition: "none" });	
	});
	
});

$(document).on("pageshow", "#page-legal", function(event) { 
    
    navigator.geolocation.getCurrentPosition(function(position) {
        reportData.longitude = position.coords.longitude;
        reportData.latitude = position.coords.latitude;
    });
    
});

