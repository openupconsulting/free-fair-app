/**************************************************************
*This file is part of Free & Fair, an electoral transparency mobile app.
*  Copyright (C) 2014 The Free & Fair Team
* 
*  Free & Fair is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  
*  Free & Fair is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************/
$(document).on("pageinit", "#page-form", function(event) { 
  
});


$(document).on("pagebeforeshow", "#page-form", function(event) { 
 console.log('pagebeforeshow FORM');
$("#page-form-title").text(localStorage.activeFormName);
 var promise = localDataLoadForms(localStorage.activeFormName);
   $.when(promise).then(function(formfields) {   
   var html = '<form action="submit_form()" name="form-generic" id="form-generic">'
       $.each(formfields, function(index, field) {
		 
		         var helptext = field.helptext;
				var rawfieldname = decodeURIComponent(field.rawfieldname)
				 var fieldname = decodeURIComponent(field.fieldname)
				 var choices = field.choices
				 if(field.formname == localStorage.activeFormName ) {
 		 
		        if(helptext.length > 0) { 
			    if(field.fieldtype == 'TEXT') {
					
						var lgth = helptext.length;
					var from = lgth - 1;
					var val; 
			     val = helptext.substring(from);
					var cls;
					   cls='';
						if (val == 'N') { cls ='class="required digits"'} 	
						if (val == 'T') { cls ='class="txt"'} 	
                   
			html += '<label for="'+ helptext+'">'+ rawfieldname+'</label>'
			html +='<fieldset class="ui-grid-a" style="width:100%">'
			html +='<div class="ui-block-a"  style="width:85%"><input type="text" id="'+ helptext+'" name="'+ helptext+'" style="width:85%" data-clear-btn="false" value="" '+cls+' /></div>'  
			html +='<div class="ui-block-b"  style="width:15%">' 
			var iplustwo =  index + 2
			if(formfields.length > iplustwo ) {
			html +=' <a href="javascript:next_input(\''+ formfields[index +1].helptext +'\')" data-role="button" data-icon="arrow-d" style="padding-top:8px; padding-right:8px;height:28px;width:28px;-moz-border-radius:14px;border-radius: 14px;" > </a>' }
			html += '</div>'
			html +='</fieldset>'
				  }
				 if(field.fieldtype == 'PARAGRAPH_TEXT') {
				  html += '<label for="'+ helptext+'">'+ rawfieldname +'</label>'
				  html +='<textarea type="text" id="'+ helptext+'" name="'+ helptext+'" type="text" rows="4" data-clear-btn="false" maxlength="120" value=""></textarea>'
  }
				  if(field.fieldtype == 'MULTIPLE_CHOICE') {
				 	    $.when(localDataLoadAppText("Choose")).then(function(text) {	
				  html += '<label for="'+ helptext+'">'+ rawfieldname +'</label>'
				  html +='<select type="text" id="'+ helptext+'" name="'+ helptext+'" class="required" type="text" rows="4" data-clear-btn="false" maxlength="120" value="">'
				   html +='<option value="">'+text+'</option>'   
				 var opts =  choices.split(",") ;
				 for(m=1; m < opts.length; m++) {
				   html +='<option> '+ decodeURIComponent(opts[m])+'</option>' }
				   html +='</select>'  
				 	});
				  } 
				}
				else {	
				var hiddenvalue;	
					if (fieldname == "placeid" )   {
					  hiddenvalue =reportData.votingSite.id ; }
					  
				if (fieldname ==  "username" )
				{	 hiddenvalue = localStorage.user_email }
					 
				if (fieldname ==  "latitude" )
				{	 hiddenvalue = reportData.latitude }
					  
				if (fieldname ==  "longitude" )
				{	 hiddenvalue = reportData.longitude }
					 
					 if (fieldname == "timerecorded" )
				{	  var d = new Date();
					 hiddenvalue =  d.getHours()+':'+d.getMinutes()+':'+d.getSeconds()}
			    html +='<input type="hidden" id="'+ fieldname+'" name="'+ fieldname+'"  data-clear-btn="false" value="'+hiddenvalue+'">'  
					}
				 } // end if formname == activeformname
			  });            
			 
	  $.when(localDataLoadAppText("Submit")).then(function(text) {	
         
          html += '<br>'
           html += '<button id="submit-form-generic"  data-icon="arrow-r" data-iconpos="right">'+ text    +'</button>'
            html += '</form>'
        	
	 $('#form-page-content').html(html); 
	 $('#form-page-content').trigger("create"); });

	    }); //promise loadform
});

$(document).on("pageshow", "#page-form", function(event) { 
   var toSend=[];
    localizeText($("#submit-form-generic"))   
   if(localStorage.networkState == 'No') {
    	$('#forms-refresh').show();
    	}    
    	else { $('#forms-refresh').hide(); }
    $("#page-form-site-title").text(reportData.votingSite.id + ' ' + reportData.votingSite.name);    
$('#submit-form-generic').live('mousedown', function(e) {
			
$("#form-generic").validate({
submitHandler: function( form ) {
        $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: '' 
        });
  
        googleapi.getToken(config.authOptions).done(function(data) {
			  $.when(localDataLoadTasks()).then(function (tasks) {	  		
		        $.each(tasks, function(index, task) {
			    if(task.menuentry == localStorage.activeFormName) {
				   localStorage.noon_participation = task.data
				   localStorage.sms_number = task.smsno
				   localStorage.taskNo = task.task
				  }
			   });     
			   toSend=[];
			  $('#form-generic *').filter(':input').each(function(index, el) {
				  console.log('To send' + el.id )
			   var element = {};
			   if (el.id !=='submit-form-generic' || el.id !=='sms-form-generic' ) {
           
		      if (el.class == 'txt') {  element[el.id] = encodeURIComponent($('#'+el.id).val());  }
		      else   { element[el.id] =  encodeURIComponent($('#'+el.id).val()); }
		      toSend.push(element);	   
			  }
            }); 
			         var element = {};   
					 element['initkey'] =  localStorage.activeInitiativeKey 
					       toSend.push(element);
						    var element = {};   
					 element['taskno'] =  localStorage.taskNo 
					       toSend.push(element);
			var promise = remoteData.saveResults(data.access_token, localStorage.noon_participation, toSend);
            $.when(promise).then(  function() { dataSent() } , function() { ThreeGSendFailed(toSend) });
        }).fail(function(data) {
				console.log('####################not sent by 3G.. trying to save for later sending ###################################');
           // ThreeGSendFailed(toSend); 
		       dataSendFailed() 
        }); 
		}); 
		}   // validation1
        }); // validation2
    }); 
	
		 function dataSent() {
        $.mobile.loading("hide");
        $.mobile.changePage("#page-task", { transition: "none" });     
		 console.log("result Datasend success")   
    }
    
    function dataSendFailed() {
    	localStorage.sendSrriptURL = "https://script.google.com/macros/s/AKfycbyZQhahpD3CdqF-ryFgIkQBQ9lMRohVisSpfPGYqXDMuQlkA_Wg/exec";
    	  console.log("lastcostdata:"+localStorage.lastPostData)
        localDataSaveUnsentResults(localStorage.sendScriptURL,  localStorage.lastPostData, localStorage.smsMessage, localStorage.sms_number );
        $.mobile.loading("hide");   
        $.mobile.changePage("#page-task", { transition: "none" });  
		 console.log("result Datasend Failed")        
    }
	
	 
	
	function  ThreeGSendFailed(toSend) { 
 	
  var 	toSendBySms=[];
  var tno;
      for(var i=0; i<toSend.length; i++) {
	  $.each( toSend[i], function(index, el) {
			   if ( index == 'taskno' ) {
				     tno = el;      
			  }
            }); 		 
	  }
  
  
	 var msg = '<'+ tno +':"'
	 for(var i=0; i<toSend.length; i++) {
   $.each( toSend[i], function(index, el) {
			   if (index != 'submit-form-generic' &&  index != 'placeid' && index !='username' && index != 'initkey'  && index != 'taskno' ) {
				//    console.log( 'id of element = #'+index )
				   if ($('#'+index).attr('type') != 'hidden') {    toSendBySms.push(decodeURIComponent(el))   } 
			  }
            }); 
	 }
	  
	  for(var i=0; i<toSend.length; i++) {
	  $.each( toSend[i], function(index, el) {
			   if ( index == 'placeid' ) {
				     toSendBySms.unshift(el)     
			  }
            }); 		 
	  }
	   for(var i=0; i<toSend.length; i++) {
			  $.each( toSend[i], function(index, el) {
			   if ( index == 'username' ) {
				     toSendBySms.unshift(decodeURIComponent(el))     
			  }
            }); 
	   }
	   
	   var msg = '<'+ localStorage.taskNo +':"'
	   	for (i=0;i< toSendBySms.length; i++) {
				 msg+= toSendBySms[i]+",";
				}
	   
	   msg+= '">'; 

	    if( msg.length <=  160 ) {  
		       
			   localStorage.smsMessage =   msg;  //  transliterate(msg)
			//  console.log(localStorage.smsMessage)  
            var intent = "INTENT"; //leave empty for sending sms using default intent
            var success =  function () {    dataSent();    };
			var failure =   function (e) {   dataSendFailed();   };
		 	sms.send( '+'+localStorage.sms_number,  localStorage.smsMessage, intent, success , failure ); 
			    $.mobile.loading("hide");
            $.mobile.changePage("#page-task", { transition: "none" });  
			  // else  dataSent();
			   }
			
			  else { 
			     alert( "Poruka nije poslata jer ima više od 160 karaktera. molimo pozovite nas i saopštite rezultate " );
			   }
			 
		
	}
	
	//************************************************************************//
    navigator.geolocation.getCurrentPosition(function(position) {
        reportData.longitude = position.coords.longitude;
        reportData.latitude = position.coords.latitude;
    });
    
});
		
	function submit_form() {
		 var a;
		 a =1;
		 };	
		 
		 
	function next_input(el) { 
	console.log(el)
	$('#'+el).focus()
	
	} 	 