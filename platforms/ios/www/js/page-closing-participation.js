$(document).on("pageinit", "#page-closing-participation", function(event) { 	

    var toSend;
    
    $('#submit-closing-participation').bind('mouseup', function(e) {
        var registered = $("#closing-registered").val();
//        if (registered != reportData.votingSite.registered_voters) {
//            $("#popup-wrong-site").popup("open", { position: "window", transition: "none" });
//        } else {
            sendData();
//        }
    });

    $("#popup-wrong-site-yes").click(function(e) {
        sendData();
    });
    
    function sendData() {
        var registered = $("#closing-registered").val();
        var voted = $("#closing-voted").val();
        var valid = $("#closing-valid-votes").val();
        var invalid = $("#closing-invalid-votes").val();
        
        $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
    
        googleapi.getToken(config.authOptions).done(function(data) {
            toSend = reportData.prepareClosingParticipationResults(registered, voted, valid, invalid);
            var promise = remoteData.saveResults(data.access_token, localStorage.closing_participation, toSend);
            $.when(promise).then(dataSent, dataSendFailed);
        }).fail(function(data) {
            dataSendFailed();
        });
    }
    
    function dataSent() {
        $.mobile.loading("hide");
        $.mobile.changePage("#page-task",  { transition: "none" });        
    }
    
    function dataSendFailed() {
        localDataSaveUnsentResults(localStorage.closing_participation, toSend);
        $.mobile.loading("hide");
        $.mobile.changePage("#page-task", { transition: "none" });        
    }	
    
});

$(document).on("pageshow", "#page-closing-participation", function(event) { 
    
    $("#page-closing-participation-title").text(reportData.votingSite.id + ' ' + reportData.votingSite.name);

    $("#closing-registered").val("");
    $("#closing-voted").val("");
    $("#closing-valid-votes").val("");
    $("#closing-invalid-votes").val("");

    navigator.geolocation.getCurrentPosition(function(position) {
        reportData.longitude = position.coords.longitude;
        reportData.latitude = position.coords.latitude;
    });
    
});