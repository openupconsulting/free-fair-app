/**************************************************************
*This file is part of Free & Fair, an electoral transparency mobile app.
*  Copyright (C) 2014 The Free & Fair Team
* 
*  Free & Fair is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  
*  Free & Fair is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************/

var reportData = {
    votingSite: {},
	activeInitiativeKey: 0, 
    longitude: 0,
    latitude: 0,
	  
    prepareNoonParticipationResults: function(votes, comment) {
        var entry = [];
		var element = {};
		  element['PlaceID'] = reportData.votingSite.id 
		  entry.push(element); 
		  var element = {};
          element['Email'] =   localStorage.user_email 
		  entry.push(element);
		   var element = {};
          element['Latitude'] = reportData.latitude 
		  entry.push(element);
		  var element = {};
          element['Longitude'] = reportData.longitude 
		  entry.push(element);
		  var element = {};
          element['Time'] = "00:00"    
		  entry.push(element);
		   var element = {};
          element['Nparticipation'] = votes 
		  entry.push(element);
		   var element = {};
		  element['initkey'] = localStorage.activeInitiativeKey
		  entry.push(element); 
		    var element = {};
		  element['Comments'] = encodeURIComponent(comment);
		  entry.push(element); 
      return entry;
    },

    prepareClosingParticipationResults: function(registered, voted, valid, invalid) {
        var entry = '<entry xmlns="http://www.w3.org/2005/Atom" ' 
            + ' xmlns:gsx="http://schemas.google.com/spreadsheets/2006/extended">'
            + '<gsx:siteid>' + reportData.votingSite.id + '</gsx:siteid>'
            + '<gsx:userid>' + localStorage.user_email + '</gsx:userid>'
            + '<gsx:latitude>' + reportData.latitude + '</gsx:latitude>'
            + '<gsx:longitude>' + reportData.longitude + '</gsx:longitude>'
            + '<gsx:timerecorded>' + this.formatDate(new Date()) + '</gsx:timerecorded>'
            + '<gsx:timesent>##TIMESTAMP##</gsx:timesent>'
            + '<gsx:registered>' + registered + '</gsx:registered>'
            + '<gsx:voted>' + voted + '</gsx:voted>'
            + '<gsx:validballots>' + valid + '</gsx:validballots>'
            + '<gsx:invalidballots>' + invalid + '</gsx:invalidballots>'
            + '</entry>';    
        return entry;
    },

    prepareFinalResults: function(votes) {
        var results = '';
        
        $.each(votes, function(name, value) {
            results = results + '<gsx:' + name + '>' + value + '</gsx:' + name + '>';
        });
        
        var entry = '<entry xmlns="http://www.w3.org/2005/Atom" ' 
            + ' xmlns:gsx="http://schemas.google.com/spreadsheets/2006/extended">'
            + '<gsx:siteid>' + reportData.votingSite.id + '</gsx:siteid>'
            + '<gsx:userid>' + localStorage.user_email + '</gsx:userid>'
            + '<gsx:latitude>' + reportData.latitude + '</gsx:latitude>'
            + '<gsx:longitude>' + reportData.longitude + '</gsx:longitude>'
            + '<gsx:timerecorded>' + this.formatDate(new Date()) + '</gsx:timerecorded>'
            + '<gsx:timesent>##TIMESTAMP##</gsx:timesent>'
            + results
            + '</entry>';
        
        return entry;
    },
    
    formatDate: function(date) {
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
    
        var hour = date.getHours();
        var minute = date.getMinutes();
        var second = date.getSeconds();
        
        return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
    }
}

