function localDataLoadAppTexts() {
    var deferred = $.Deferred();

    var appTexts = [];
    
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('SELECT * FROM app_texts', [], querySuccess, deferred.reject);			
    });
    
    var querySuccess = function(tx, result) {
        var len = result.rows.length;
        for (var i = 0; i < len; i++) {
            var item = result.rows.item(i);
            appTexts.push(item);
        }
        deferred.resolve(appTexts);
    }
    
    return deferred.promise();
}

	
function localDataLoadAppText(key) {
    var deferred = $.Deferred();
    
    $.when(localDataLoadAppTexts()).then(function(appTexts) {
        var found = false;
        $.each(appTexts, function(index, appText) {
            if (appText.key === key) {
                deferred.resolve(appText.value);
                found = true;
            }
        });                  
        
        if (!found) deferred.resolve("");
    }, deferred.reject);
    
    return deferred.promise();
}
	
function localDataSaveAppTexts(appTexts) {
    var deferred = $.Deferred();
            
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS app_texts (key, value)');

        tx.executeSql('DELETE FROM app_texts');

        $.each(appTexts, function(index, appText) {
            tx.executeSql('INSERT INTO app_texts (key, value) VALUES ("' + appText.key + '","' + appText.value  + '")');
        });

    }, deferred.reject, deferred.resolve);
    
    return deferred.promise();
}
	
function localDataLoadBallotDetails() {
    var deferred = $.Deferred();

    var ballotDetails = [];
    
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('SELECT * FROM ballot_details', [], querySuccess, deferred.reject);			
    });
    
    var querySuccess = function(tx, result) {
        var len = result.rows.length;
        for (var i = 0; i < len; i++) {
            ballotDetails.push(result.rows.item(i));
        }
        deferred.resolve(ballotDetails);
    }
    
    return deferred.promise();
}
	
function localDataSaveBallotDetails(ballotDetails) {
    var deferred = $.Deferred();
            
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS ballot_details (id, name)');

        tx.executeSql('DELETE FROM ballot_details');

        $.each(ballotDetails, function(index, list) {
            tx.executeSql('INSERT INTO ballot_details (id, name) VALUES ("' + list.id + '","' + list.name  + '")');
        });

    }, deferred.reject, deferred.resolve);
    
    return deferred.promise();
}
	
function localDataLoadVotingSites() {
    var deferred = $.Deferred();

    var votingSites = [];
    
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('SELECT * FROM voting_sites', [], querySuccess, deferred.reject);			
    });
    
    var querySuccess = function(tx, result) {
        var len = result.rows.length;
        for (var i = 0; i < len; i++) {
            votingSites.push(result.rows.item(i));
        }
        deferred.resolve(votingSites);
    }
    
    return deferred.promise();
}

function localDataSaveVotingSites(votingSites) {
    var deferred = $.Deferred();
            
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS voting_sites' 
                      + ' (id, name, address, longitude, latitude, zipcode, city, state, registered_voters)');

        tx.executeSql('DELETE FROM voting_sites');

        $.each(votingSites, function(index, site) {
            tx.executeSql('INSERT INTO voting_sites ' 
                          + '(id, name, address, longitude, latitude, zipcode, city, state, registered_voters) ' 
                          + ' VALUES ("' + site.id + '","' + site.name + '","' + site.address + '","' 
                          + site.longitude + '","' + site.latitude + '","' + site.zipcode + '","' 
                          + site.city + '","' + site.state + '","' + site.registeredvoters + '")');
        });

    }, deferred.reject, deferred.resolve);
    
    return deferred.promise();
}

function localDataLoadUnsentResults() {
    var deferred = $.Deferred();

    var unsentResults = [];
    
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('SELECT rowid, * FROM unsent_results', [], querySuccess, deferred.reject);			
    });
    
    var querySuccess = function(tx, result) {
        var len = result.rows.length;
        for (var i = 0; i < len; i++) {
            unsentResults.push(result.rows.item(i));
        }
        deferred.resolve(unsentResults);
    }
    
    return deferred.promise();
}
	
function localDataSaveUnsentResults(url, data) {
    var deferred = $.Deferred();
            
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS unsent_results (url, data)");
        tx.executeSql("INSERT INTO unsent_results (url, data) VALUES ('" + url + "','" + data + "')");
    }, deferred.reject, deferred.resolve);
    
    return deferred.promise();
}

function localDataDeleteUnsentResults(id) {
    var deferred = $.Deferred();
            
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql("DELETE FROM unsent_results WHERE ROWID = " + id);
    }, deferred.reject, deferred.resolve);
    
    return deferred.promise();
}

function localDataOpenDatabase() {
    return window.openDatabase("elections", "1", "Elections", 2000000);
}