$(document).on("pageinit", "#page-site", function (event) {
	    
    var votingSites = [];
    
    var sortedById = true;
    
    function initData() {
        var items = "";
        
        $.when(localDataLoadVotingSites()).then(function (sites) {
            votingSites = sites;
            
            if (sortedById) {
                votingSites.sort(sortById);
            } else {
                votingSites.sort(sortByDistance);
            }
            
            $.each(votingSites, function (index, site) {
                items += '<li data-name="' + site.id + '"><a href="#">' + site.id + ' ' + site.name + '</a></li>';
            });
            
            $("#voting-site-list").html(items);
            $("#voting-site-list").listview("refresh");
            
            $('#voting-site-list').children('li').bind('mouseup', function (e) {
                var siteId = $(this).attr('data-name');

                $.each(votingSites, function (i, s) {
                    if (s.id === siteId) {
                        reportData.votingSite = s;
                        return;
                    }
                });

                $.mobile.changePage("#page-task", { transition: "none" });
            });
        });
    }

    initData(); //first run
        
    $('#site-sort-by-id').click(function (e) {
        sortedById = true;
        initData();
    });
                            
    $('#site-sort-by-distance').click(function (e) {
        sortedById = false;
        initData();
    });
    
    function sortById(site1, site2) {
        var id1 = parseInt(site1.id);
        var id2 = parseInt(site2.id);
        return id1 - id2;
    }
    
    function sortByDistance(site1, site2) {
        var lon1 = parseFloat(site1.longitude);
        var lat1 = parseFloat(site1.latitude);

        var lon2 = parseFloat(site2.longitude);
        var lat2 = parseFloat(site2.latitude);

        var lonMe = parseFloat(reportData.longitude);
        var latMe = parseFloat(reportData.latitude);

        return distance(lon1, lat1, lonMe, latMe) - distance(lon2, lat2, lonMe, latMe);
    }
    
    function distance(lon1, lat1, lon2, lat2) {
        /** Converts numeric degrees to radians */
        if (typeof(Number.prototype.toRad) === "undefined") {
            Number.prototype.toRad = function() {
                return this * Math.PI / 180;
            }
        }
        
        var R = 6371; // km
        var dLat = (lat2 - lat1).toRad();
        var dLon = (lon2 - lon1).toRad();
        var lat1 = lat1.toRad();
        var lat2 = lat2.toRad();
        
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2); 
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c;
        
        return d;
    }
    
    $('#voting-sites-refresh').bind('mouseup', function (e) {
        $("#voting-site-list").html("");

        $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
    
        googleapi.getToken(config.authOptions).done(function (data) {
            remoteData.loadData(data.access_token, function () {
                var promise = localDataLoadAppTexts();
                
                $.when(promise).then(function (data) {
                    localization = data;
                    //localizeApp();      
                    dataLoaded();
                });
            }, dataLoadFailed);
        }).fail(function (data) {
            dataLoadFailed();
        });
    });

    function dataLoaded() {
        localStorage.data_loaded = "loaded";
        $.mobile.loading("hide");
        initData();
    }
    
    function dataLoadFailed() {
        $.mobile.loading("hide");
        $("#site-data-load-error").popup("open", { transition: "none", position: "window" });
        initData();
    }

    $('#submit-unsent-from-site').bind('mouseup', function (e) {
        $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
        
        var promise = localDataLoadUnsentResults();
        
        $.when(promise).then(function (unsent) {
            var sendPromise = unsentDataHandler.send(unsent);
            $.when(sendPromise).then(dataSent, dataSendFailed);
        });
    });
    
    function dataSent() {
        $.mobile.loading("hide");
        $("#div-submit-unsent-from-site").hide();            
    }
    
    function dataSendFailed() {
        $.mobile.loading("hide");
        $("#site-data-send-error").popup("open", { transition: "none", position: "window" });
    }
});

$(document).on("pageshow", "#page-site", function (event) { 
        
    var promise = localDataLoadUnsentResults();
    
    $.when(promise).then(function (unsentResults) {
        if (unsentResults.length > 0) {
            $("#div-submit-unsent-from-site").show();            
            unsentSite = unsentResults;
        } else {
            $("#div-submit-unsent-from-site").hide();            
        }
    });
        
    navigator.geolocation.getCurrentPosition(function (position) {
        reportData.longitude = position.coords.longitude;
        reportData.latitude = position.coords.latitude;

        if (reportData.longitude == 0 || reportData.latitude == 0) {
            $("#site-sorting-button-group").hide();
            $("#voting-site-list").prev("form.ui-listview-filter").show();
        } else {
            $("#site-sorting-button-group").show();
            $("#voting-site-list").prev("form.ui-listview-filter").hide();
        }
    });
    
    if (reportData.longitude == 0 || reportData.latitude == 0) {
        $("#site-sorting-button-group").hide();
        $("#voting-site-list").prev("form.ui-listview-filter").show();
    } else {
        $("#site-sorting-button-group").show();
        $("#voting-site-list").prev("form.ui-listview-filter").hide();
    }

});
