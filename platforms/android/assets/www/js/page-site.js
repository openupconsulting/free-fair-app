/**************************************************************
*This file is part of Free & Fair, an electoral transparency mobile app.
*  Copyright (C) 2014 The Free & Fair Team
* 
*  Free & Fair is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  
*  Free & Fair is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************/
$(document).on("pageinit", "#page-site", function (event) {
	 	 $.when(localDataLoadAppText("Refresh")).then(function(text) {
        $("#voting-sites-refresh").text(text); });  
	//    console.log('ENTERING page site');
    var votingSites = [];
    var sortedById = true;
	function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i].trim() == needle.trim()) return true;
    }
    return false;
}
    
    function initData() {
        var items = "";
        
        $.when(localDataLoadVotingSites()).then(function (sites) {
            votingSites = sites;
            
            if (sortedById) {
                votingSites.sort(sortById);
            } else {
                votingSites.sort(sortByDistance);
            }
          //  console.log('THIS USERS SITES ARE: ' + localStorage.pollSites)
            $.each(votingSites, function (index, site) {
				var pSites =  localStorage.pollSites.split(",") ;
				if(inArray(site.id, pSites)) {
                items += '<li data-name="' + site.id + '"><a href="#">' + site.id + ' ' + site.name + '</a></li>'; }
            });
            
            $("#voting-site-list").html(items);
            $("#voting-site-list").listview("refresh");
            $('#voting-site-list').children('li').bind('mouseup', function (e) {
                var siteId = $(this).attr('data-name');
                $.each(votingSites, function (i, s) {
                    if (s.id === siteId) {
                        reportData.votingSite = s;
                        return;
                    }
                });
                $.mobile.changePage("#page-task", { transition: "none" });
            });
        });
    }

    initData(); //first run
        
      
    $('#site-sort-by-id').click(function (e) {
        sortedById = true;
        initData();
    });
                            
    $('#site-sort-by-distance').click(function (e) {
        sortedById = false;
        initData();
    });
    
    function sortById(site1, site2) {
        var id1 = parseInt(site1.id);
        var id2 = parseInt(site2.id);
        return id1 - id2;
    }
    
    function sortByDistance(site1, site2) {
        var lon1 = parseFloat(site1.longitude);
        var lat1 = parseFloat(site1.latitude);
        var lon2 = parseFloat(site2.longitude);
        var lat2 = parseFloat(site2.latitude);
        var lonMe = parseFloat(reportData.longitude);
        var latMe = parseFloat(reportData.latitude);
        return distance(lon1, lat1, lonMe, latMe) - distance(lon2, lat2, lonMe, latMe);
    }
    
    function distance(lon1, lat1, lon2, lat2) {
        /** Converts numeric degrees to radians */
        if (typeof(Number.prototype.toRad) === "undefined") {
            Number.prototype.toRad = function() {
                return this * Math.PI / 180;
            }
        }
        
        var R = 6371; // km
        var dLat = (lat2 - lat1).toRad();
        var dLon = (lon2 - lon1).toRad();
        var lat1 = lat1.toRad();
        var lat2 = lat2.toRad();
        
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2); 
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c;
        return d;
    }
   
    
    $('#voting-sites-refresh').bind('mouseup', function (e) {
        $("#voting-site-list").html("");
        $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
    
        googleapi.getToken(config.authOptions).done(function (data) {
            remoteData.loadData(data.access_token, function () {
                var promise = localDataLoadAppTexts();
                $.when(promise).then(function (data) {
                    localization = data;
                    localizeApp();  
                    initData(); 
                    $.mobile.loading("hide");     
                });
            }, dataLoadFailed);
        }).fail(function (data) {
            dataLoadFailed();
        });
    });

    function dataLoaded() {
        localStorage.data_loaded = "loaded";
        $.mobile.loading("hide");
        initData();
    }
    
    function dataLoadFailed() {
        $.mobile.loading("hide");
        $("#site-data-load-error").popup("open", { transition: "none", position: "window" });
        initData();
    }

    $('#submit-unsent-from-site').bind('mouseup', function (e) {
        $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
        
        var promise = localDataLoadUnsentResults();
        
        $.when(promise).then(function (unsent) {
            var sendPromise = unsentDataHandler.send(unsent);
            $.when(sendPromise).then(dataSent, dataSendFailed);
        });
    });
    
    function dataSent() {
        $.mobile.loading("hide");
        $("#div-submit-unsent-from-site").hide();            
    }
    
    function dataSendFailed() {
        $.mobile.loading("hide");
        $("#site-data-send-error").popup("open", { transition: "none", position: "window" });
    }
});

$(document).on("pageshow", "#page-site", function (event) { 
   navigator.geolocation.getCurrentPosition(function (position) {
       
	    reportData.longitude = position.coords.longitude;
        reportData.latitude = position.coords.latitude;

        if (reportData.longitude == 0 || reportData.latitude == 0) {
        //	 console.log('GPS lat = :' + reportData.latitude )
            $("#site-sorting-button-group").hide();
            $("#voting-site-list").prev("form.ui-listview-filter").show();
        } else {
		//	 console.log('GPS lat = :' + reportData.latitude )
            $("#site-sorting-button-group").show();
            $("#voting-site-list").prev("form.ui-listview-filter").hide();
        }
    	}, function(error) {
			
    //    console.log('GPS err code: '    + error.code    + '\n' +
      //        'message: ' + error.message + '\n');
    		
    		});
    
        remoteData.checkConnection();
    if(localStorage.networkState == 'No') {
    
    	$("#div-refresh").hide();
    	}    
    	else { 
    	//	console.log("showing site refresh")
    		
    		$("#div-refresh").show(); }
  
      
    var promise = localDataLoadUnsentResults();
    
    $.when(promise).then(function (unsentResults) {
        if (unsentResults.length > 0) {
            $("#div-submit-unsent-from-site").show();            
            unsentSite = unsentResults;
        } else {
            $("#div-submit-unsent-from-site").hide();            
        }
    });
        
  

});
