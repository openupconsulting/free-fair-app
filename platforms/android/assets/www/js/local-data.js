/**************************************************************
*This file is part of Free & Fair, an electoral transparency mobile app.
*  Copyright (C) 2014 The Free & Fair Team
* 
*  Free & Fair is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  
*  Free & Fair is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************/
function localDataLoadAppTexts() {
    var deferred = $.Deferred();

    var appTexts = [];
    
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('SELECT * FROM app_texts', [], querySuccess, deferred.reject);			
    });
    
    var querySuccess = function(tx, result) {
        var len = result.rows.length;
        for (var i = 0; i < len; i++) {
            var item = result.rows.item(i);
            appTexts.push(item);
        }
        deferred.resolve(appTexts);
    }
      return deferred.promise();
}

	
function localDataLoadAppText(key) {
    var deferred = $.Deferred();
    
    $.when(localDataLoadAppTexts()).then(function(appTexts) {
        var found = false;
        $.each(appTexts, function(index, appText) {
            if (appText.key === key) {
                deferred.resolve(appText.value);
                found = true;
            }
        });                  
        
        if (!found) deferred.resolve("");
    }, deferred.reject);
    
    return deferred.promise();
}
	
function localDataSaveAppTexts(appTexts) {
    var deferred = $.Deferred();
            
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS app_texts (key, value)');
        tx.executeSql('DELETE FROM app_texts');
        $.each(appTexts, function(index, appText) {
            tx.executeSql('INSERT INTO app_texts (key, value) VALUES ("' + appText.key + '","' + appText.value  + '")');
        });
    }, deferred.reject, deferred.resolve);
    return deferred.promise();
}
	
function localDataLoadVotingSites() {
    var deferred = $.Deferred();

    var votingSites = [];
    
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('SELECT * FROM voting_sites', [], querySuccess, deferred.reject);			
    });
    
    var querySuccess = function(tx, result) {
        var len = result.rows.length;
        for (var i = 0; i < len; i++) {
            votingSites.push(result.rows.item(i));
			
        }
        deferred.resolve(votingSites);
    }
    
    return deferred.promise();
}

function localDataLoadTasks() {
    var deferred = $.Deferred();

    var tasks = [];
    
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('SELECT * FROM tasks', [], querySuccess, deferred.reject);			
    });
    
    var querySuccess = function(tx, result) {
        var len = result.rows.length;
        for (var i = 0; i < len; i++) {
            tasks.push(result.rows.item(i));
			//  console.log("localy pushed task:" + result.rows.item(i).task )
        }
        deferred.resolve(tasks);
    }
    
    return deferred.promise();
}


function localDataLoadInitiatives() {

    var deferred = $.Deferred();

    var initiatives = [];
    
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('SELECT * FROM ONGOING', [], querySuccess, deferred.reject);			
    });
   
    var querySuccess = function(tx, result) {
		// console.log('local data loading initiatives. rows:' +  result.rows.length)
        var len = result.rows.length;
        for (var i = 0; i < len; i++) {
            initiatives.push(result.rows.item(i));
        }
        deferred.resolve(initiatives);
    }
    
    return deferred.promise();
}


function localDataLoadForms(formname) {

    var deferred = $.Deferred();

    var formfields = [];
    
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql("SELECT * FROM FORMS", [], querySuccess, deferred.reject);			
    });
   
    var querySuccess = function(tx, result) {
		
        var len = result.rows.length;
        for (var i = 0; i < len; i++) {
             formfields.push(result.rows.item(i));
        }
        deferred.resolve(formfields);
    }
    
    return deferred.promise();
}



function localDataSaveVotingSites(votingSites) {
    var deferred = $.Deferred();
            
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS voting_sites' 
                      + ' (id, name, address, longitude, latitude, zipcode, region, county, registered_voters)');

        tx.executeSql('DELETE FROM voting_sites');

        $.each(votingSites, function(index, site) {
            tx.executeSql('INSERT INTO voting_sites ' 
                          + '(id, name, address, longitude, latitude, zipcode, region, county, registered_voters) ' 
                          + ' VALUES ("' + site.id + '","' + site.name + '","' + site.address + '","' 
                          + site.longitude + '","' + site.latitude + '","' + site.zipcode + '","' 
                          + site.region + '","' + site.county + '","' + site.registeredvoters + '")');
						
        });

    }, deferred.reject, deferred.resolve);
    
    return deferred.promise();
}

function localDataSaveTasks(tasks) {
    var deferred = $.Deferred();
            
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS tasks' 
                      + '(task, menuentry, type, data, smsno)');

        tx.executeSql('DELETE FROM tasks');

        $.each(tasks, function(index, task) {
            tx.executeSql('INSERT INTO tasks' 
                          + '(task, menuentry, type, data, smsno) ' 
                          + ' VALUES ("' + task.task + '","' + task.menuentry + '","' + task.type + '","' 
                          + task.data + '","'+ task.sms + '")');
						
        });

    }, deferred.reject, deferred.resolve);
    
    return deferred.promise();
}

function localDataSaveInitiatives(initiatives) {
    var deferred = $.Deferred();
    //  console.log('entering localdatasaveinitiatives')      
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS ONGOING' 
                      + ' (country, initiative, menu_entry, date, expiration, key)');

        tx.executeSql('DELETE FROM ONGOING');

        $.each(initiatives, function(index, initiative) {
            tx.executeSql('INSERT INTO ONGOING ' 
                          + '(country, initiative, menu_entry, date, expiration, key) ' 
                          + ' VALUES ("' + initiative.country + '","' + initiative.initiative + '","' + initiative.menu + '","' 
                          + initiative.date + '","' + initiative.expiration + '","' + initiative.key + '")');
						 //  console.log('inserted ' + initiative.initiative + ' '+ initiative.key);
        });
        
    }, deferred.reject, deferred.resolve);
    
    return deferred.promise();
}


function localDataSaveForms(forms) {
    var deferred = $.Deferred();
     // console.log('entering localdata saveForms')      
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS FORMS' 
                      + ' (formname, fieldname, rawfieldname, fieldtype, helptext, choices)');

        tx.executeSql('DELETE FROM FORMS');
        
        $.each(forms, function(index, form) {
 
		         $.each(form, function(index, field) {
					 var title = decodeURIComponent(field.title);
					 var rawtitle = decodeURIComponent(field.rawtitle);
            tx.executeSql('INSERT INTO FORMS' 
                          + '(formname, fieldname, rawfieldname, fieldtype, helptext, choices)' 
                          + ' VALUES ("' + field.formname +   '","' + field.title + '","' + field.rawtitle + '","' + field.type + '","' 
                          + field.helptxt + '","' + field.choices +  '")');
						//   console.log('inserted ' + field.formname + ' '+ field.title + ' '+ field.rawtitle);
						    });
        });
        
    }, deferred.reject, deferred.resolve);
    
    return deferred.promise();
}



function localDataLoadUnsentResults() {
    var deferred = $.Deferred();

    var unsentResults = [];
    
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql('SELECT rowid, * FROM unsent_results', [], querySuccess, deferred.reject);			
    });
    
    var querySuccess = function(tx, result) {
        var len = result.rows.length;
        for (var i = 0; i < len; i++) {
            unsentResults.push(result.rows.item(i));
        }
         
        console.log('unsent results loaded form local: ' + JSON.stringify(unsentResults[0]))
        deferred.resolve(unsentResults);
    }
    
    return deferred.promise();
}
	
function localDataSaveUnsentResults(url, dta, msg, smsno) {
    var deferred = $.Deferred();
            
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS unsent_results (url, data, msg, smsno)");
        tx.executeSql("INSERT INTO unsent_results (url, data, msg, smsno) VALUES ('" + url + "','" + dta + "','" + msg +"','" + smsno +"')");
    }, deferred.reject, deferred.resolve);
    
    return deferred.promise();
}

function localDataDeleteUnsentResults(id) {
    var deferred = $.Deferred();
            
    var db = localDataOpenDatabase();
    
    db.transaction(function(tx) {
        tx.executeSql("DELETE FROM unsent_results WHERE ROWID = " + id);
    }, deferred.reject, deferred.resolve);
    
    return deferred.promise();
}

function localDataOpenDatabase() {
    return window.openDatabase("elections", "1", "Elections", 2000000);
}


function transliterate(word){
    var answer = ""
      , a = {};

   a["Њ"]="Nj";a["Ђ"]="Dj";a["Ц"]="C";a["У"]="U";a["К"]="K";a["Е"]="E";a["Н"]="N";a["Г"]="G";a["Ш"]="S";a["Ž"]="Z";a["З"]="Z";a["Х"]="H";a["Ъ"]="'";
   a["њ"]="nj";a["ђ"]="dj";a["ц"]="c";a["у"]="u";a["к"]="k";a["е"]="e";a["н"]="n";a["г"]="g";a["ш"]="s";a["ž"]="z";a["з"]="z";a["х"]="h";a["ъ"]="'";
   a["Ф"]="F";a["Ć"]="C";a["В"]="V";a["А"]="a";a["П"]="P";a["Р"]="R";a["О"]="O";a["Л"]="L";a["Д"]="D";a["Ж"]="Z";a["Č"]="c";
   a["ф"]="f";a["ć"]="c";a["в"]="v";a["а"]="a";a["п"]="p";a["р"]="r";a["о"]="o";a["л"]="l";a["д"]="d";a["ж"]="z";a["č"]="c";
   a["Љ"]="Lj";a["Ч"]="C";a["С"]="S";a["М"]="M";a["И"]="I";a["Т"]="T";a[""]="'";a["Б"]="B";a["Š"]="s";a["Đ"]="Dj";a["Џ"]="Dz";a["У"]="U";a["Ћ"]="C";a["ћ"]="c";
   a["љ"]="lj";a["ч"]="c";a["с"]="s";a["м"]="m";a["и"]="i";a["т"]="t";a["ь"]="'";a["б"]="b";a["š"]="s";a["đ"]="dj";a["x"]="Dz";a["у"]="u";a["џ"]="dz"

   for (i in word){
     if (word.hasOwnProperty(i)) {
       if (a[word[i]] === undefined){
         answer += word[i];
       } else {
         answer += a[word[i]];
       }
     }
   }
   return answer;
}