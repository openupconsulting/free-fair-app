$(document).on("pageinit", "#page-legal", function(event) { 	
	
    $.when(localDataLoadAppText("Warning text")).then(function(text) {
        $("#legalText").text(text);
    });
     $.when(localDataLoadAppText("Accept and continue")).then(function(text) {
        $("#legal-accept").text(text);
    });
	
	$("#legal-accept").click(function(event) {
        localStorage.legal_accepted = "accepted";
		 $.mobile.changePage("#page-site", { transition: "none" });	
	});
	
});

$(document).on("pageshow", "#page-legal", function(event) { 
    
    navigator.geolocation.getCurrentPosition(function(position) {
        reportData.longitude = position.coords.longitude;
        reportData.latitude = position.coords.latitude;
    });
    
});

