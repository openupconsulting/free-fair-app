$(document).on("pageinit", "#page-initiatives", function (event) {
	    
	    remoteData.checkConnection();
	   console.log('entering page initiative connection is :' + localStorage.networkState)
    var initiatives = [];
    $.when(localDataLoadAppText("Select initiative")).then(function(text) {
        $("#page-initiative-title").text(text);
        
			    if(localStorage.activeInitiativeKey ) {
			    	
			    	 var promise = localDataLoadAppTexts();
                $.when(promise).then(function (data) {
                    localization = data;
                    localizeApp(); //   dataLoaded(); was here          
                });

			    if (localStorage.legal_accepted === "accepted") {
			    $.mobile.changePage("#page-site", { transition: "none" });
						
            } else {
                $.mobile.changePage("#page-legal", { transition: "none" });
            }  	  
			  }
    });
   
  
    function initData() {
        var items = "";
        
        $.when(localDataLoadInitiatives()).then(function (initiatives) {
              ongoingInitiatives = initiatives;
               console.log('loadinitiative from local db has finished. building list....')
 
            // building list
            $.each(ongoingInitiatives, function (index, init) {
                items += '<li data-name="' + init.key + '"><a href="#">' + init.initiative + ' </a></li>';
                console.log('building list:' + init.key + ' '+ init.initiative)
		    });
            
            $("#initiatives-list").html(items);
            $("#initiatives-list").listview("refresh");
			
        
		    $('#initiatives-list').children('li').bind('mouseup', function (e) {
                var PrevInitiativeKey = localStorage.activeInitiativeKey ;
			    var InitiativeKey = $(this).attr('data-name');
                 localStorage.activeInitiativeKey = $(this).attr('data-name');
			 	$.each(ongoingInitiatives, function (i, s) {	
			       if (s.key ==  InitiativeKey ) {   }
			         });			
		 if (localStorage.data_loaded != "loaded") {  // tables are not loaded locally 
		  
		    $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
		 remoteData.loadData(localStorage.access_token, function()
		  {  console.log('success remotedata.loaddata')  
		    var promise = localDataLoadAppTexts();
		    $.when(promise).then(function(data) {
			      localization = data;
                    localizeApp();   
			   console.log('Localdata LoadAppTexts Done!')  
			    if (localStorage.legal_accepted === "accepted") {
			    $.mobile.changePage("#page-site", { transition: "none" });
						
            } else {
                $.mobile.changePage("#page-legal", { transition: "none" });
            }  
		         var promise = localDataLoadVotingSites();
				  $.when(promise).then(function(data) {    console.log('Localdata LoadVotingSites Done!') 	
				  	}, function() {}); 
					localStorage.data_loaded = "loaded";
        }, function() {});
		   },  function() {});   
		 }
		 
		 else {  // tables are loaded locally
		  $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
		   var promise = localDataLoadAppTexts();
		    $.when(promise).then(function(data) {
			      localization = data;
          
			   console.log('Localdata LoadAppTexts Done!')  
		         var promise = localDataLoadVotingSites();
				  $.when(promise).then(function(data) {  
				  	localizeApp();  
				   var tasksPromise =  localDataLoadTasks(); 
				   var formsPromise = 	 localDataLoadForms(); 
				      $.when(tasksPromise, formsPromise)
               .then(function() {  console.log('Localdata LoadVotingSites Done!') 
              $.mobile.loading("hide");   
        	  if (localStorage.legal_accepted === "accepted") {
			    $.mobile.changePage("#page-site", { transition: "none" });
						
            } else {
                $.mobile.changePage("#page-legal", { transition: "none" });
            }                	
                    	
                    	}, function(){  $.mobile.loading("hide"); }); 

			 
        });
		   },  function() { $.mobile.loading("hide");});
		      
		  }
            });  // end bind mouseup	v
        });
    }
    initData(); //first run
        
    $('#initiatives-refresh').bind('mouseup', function (e) {
        $("#initiatives-list").html("");

        $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
        
        googleapi.getToken(config.authOptions).done(function (data) {
				initData(); 
        }); 
    });

    function dataLoaded() {
        localStorage.data_loaded = "loaded";
        $.mobile.loading("hide");
        initData();
    }
    
    function dataLoadFailed() {
        $.mobile.loading("hide");
        $("#initiatives-data-load-error").popup("open", { transition: "none", position: "window" });
        initData();
    } 
    function dataSendFailed() {
        $.mobile.loading("hide");
        $("#site-data-send-error").popup("open", { transition: "none", position: "window" });
    }
});
var localization;
function localizeApp() {
    $("h1, button, a, label, p, h2").each(function() {
        localizeText($(this));
    });
}
function localizeText(el) {
    var l = findLocalization(el.text());
    if (l != null) {
        el.text(l);
    }
}

function findLocalization(key) {
    var result = null;
    $.each(localization, function(i, l) {
        if (l.key == key) {
            result = l.value;
            return;
        }
    });
    return result;
}

$(document).on("pageshow", "#page-initiatives", function(event) { 
	  remoteData.checkConnection();
	 if(localStorage.networkState == 'No') {
    	 console.log('page init show: connection is'+ localStorage.networkState )
    	$("#div-refresh-pinit").hide();
    	}    
    	else { $("#div-refresh-pinit").show(); }   
});

