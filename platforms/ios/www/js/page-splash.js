$(document).on('deviceready', function() {
    
    $(document).on("pageshow", "#page-splash", function(event) { 
        $("#electionsapp").css('visibility', 'visible');
        initData();
        
        navigator.geolocation.getCurrentPosition(function(position) {
            reportData.longitude = position.coords.longitude;
            reportData.latitude = position.coords.latitude;
        });
    });

    function initData() {            
        if (localStorage.data_loaded === "loaded") {
            dataLoaded();
            return;
        }
        
        $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
    
        googleapi.getToken(config.authOptions).done(function(data) {
            remoteData.loadData(data.access_token, dataLoaded, dataLoadFailed);
        }).fail(function(data) {
            dataLoadFailed();
        });
    } 

    function dataLoaded() {
        localStorage.data_loaded = "loaded";
        
        var promise = localDataLoadAppTexts();
        
        $.when(promise).then(function(data) {
            localization = data;
            localizeApp();
            
            if (localStorage.legal_accepted === "accepted") {
                $.mobile.changePage("#page-site", { transition: "none" });
            } else {
                $.mobile.changePage("#page-legal", { transition: "none" });
            }            
        });
    }
    
    function dataLoadFailed() {
        initInProgress = false;
        
        $.mobile.loading("hide");
        $("#splash-data-load-error").popup("open", { transition: "none", position: "window" });
    }	
    
});

var localization;

function localizeApp() {
    $("h1, button, a, label, p, h2").each(function() {
        localizeText($(this));
    });
}

function localizeText(el) {
    var l = findLocalization(el.text());
    if (l != null) {
        el.text(l);
    }
}

function findLocalization(key) {
    var result = null;
    $.each(localization, function(i, l) {
        if (l.key == key) {
            result = l.value;
            return;
        }
    });
    return result;
}