$(document).on("pageinit", "#page-task", function(event) { 	
    
	$('#task-list').children('li').bind('mouseup', function(e) {
		$.mobile.changePage("#" + $(this).attr('data-name'), { transition: "none" });
	});
	
    $('#submit-unsent-from-task').bind('mouseup', function(e) {
        $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
        
        var promise = localDataLoadUnsentResults();
        
        $.when(promise).then(function(unsent) {
            var sendPromise = unsentDataHandler.send(unsent);
            $.when(sendPromise).then(dataSent, dataSendFailed);
        });
    });

    function dataSent() {
        $.mobile.loading("hide");
        $("#div-submit-unsent-from-task").hide();            
    }
    
    function dataSendFailed() {
        $.mobile.loading("hide");
        $("#task-data-send-error").popup("open", { transition: "none", position: "window" });        
    }
});

$(document).on("pageshow", "#page-task", function(event) { 
        
    $("#page-task-title").text(reportData.votingSite.id + ' ' + reportData.votingSite.name);

    var promise = localDataLoadUnsentResults();
    
    $.when(promise).then(function(unsentResults) {
        if (unsentResults.length > 0) {
            $("#div-submit-unsent-from-task").show();            
            unsentTask = unsentResults;
        } else {
            $("#div-submit-unsent-from-task").hide();            
        }
    });
    
    navigator.geolocation.getCurrentPosition(function(position) {
        reportData.longitude = position.coords.longitude;
        reportData.latitude = position.coords.latitude;
    });
    
});
