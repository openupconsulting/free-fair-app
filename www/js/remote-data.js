var remoteData = { 
	tables: { },
	    
	loadData: function(accessToken, successCallback, errorCallback) {
        remoteData.loadEmail(accessToken, function() {
            remoteData.loadDoc(accessToken, function(uri) {
                remoteData.loadTables(accessToken, uri, function(tables) {
					  var sitesPromise = remoteData.loadVotingSites(accessToken);
            var textsPromise = remoteData.loadAppTexts(accessToken);
					  var tasksPromise = remoteData.loadTasks(accessToken);
					  var formsPromise = remoteData.loadForms(accessToken, localStorage.activeInitiativeKey); 
					
					     remoteData.loadUsers(accessToken);
                    
                    $.when(sitesPromise, textsPromise, tasksPromise, formsPromise)
                    .then(successCallback, errorCallback);
                }, errorCallback);
            }, errorCallback);
        }, errorCallback);
	},
	
    loadEmail: function(accessToken, callback, errorCallback) {
		$.ajax({
			type: "GET",
			url: "https://www.googleapis.com/userinfo/email?alt=json",
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0" 
			}
		}).done(function(data) {
			console.log('loading e mail remotedata')
            localStorage.user_email = data.data.email;
            callback();
		}).fail(function(response) {
			console.log('failed loading e mail remotedata')
            errorCallback();
		});		
    },

	loadDoc: function(accessToken, callback, errorCallback) {
		$.ajax({
			type: "GET",
			url: "https://spreadsheets.google.com/feeds/spreadsheets/private/full",
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0" 
			}
		}).done(function(data) {
            
			$(data).find("entry").each(function() {
				var title = $(this).find("title").text();
				 $(this).find("link").each(function() {  
				  
				 if($(this).attr("rel") == 'alternate' ) {
				//	  console.log("ALTERNATE---->" + $(this).attr("href"));  
				 docUri = $(this).attr("href"); }
				 });
				 
			//	 console.log("URL in remotedata loaddoc: " + docUri);
				if (docUri == "https://spreadsheets.google.com/ccc?key="+ localStorage.activeInitiativeKey ) { 
				//     console.log("IT IS A MATCH----->https://spreadsheets.google.com/ccc?key="+ localStorage.activeInitiativeKey) 
					var tablesUri = $(this).find("content").first().attr("src");
					callback(tablesUri);
				}
			});
		}).fail(function(response) {
				console.log('failed loaddoc remotedata')
            errorCallback();
		});		
	},
	
	loadTables: function(accessToken, spreadsheetUri, callback, errorCallback) {
		$.ajax({
			type: "GET",
			url: spreadsheetUri,
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0"
			}
		}).done(function(data) {
			tables = { };

            var noonPromise;
            var closingPromise;
            var finalPromise;
			var usersPromise;
            
			$(data).find("entry").each(function() {				
				var title = $(this).find("title").text();
				var uri = $(this).find("content").attr("src");

				  if (title === "Users") {
                    usersPromise = remoteData.loadPostUri(accessToken, uri, "final_results");                    
                }
                
				tables[title] = uri;
			});
			
            $.when(usersPromise).then(function() {
                callback(tables);
            });
		}).fail(function(response) {
            errorCallback();
        });
	}, 
	
    loadPostUri: function(accessToken, uri, localStorageKey) {
        var deferred = $.Deferred();
        
        $.ajax({
			type: "GET",
			url: uri,
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0"
			}
		}).done(function(data) {
			$(data).find("link").each(function() {	
                if ($(this).attr("rel") === "http://schemas.google.com/g/2005#post") {
                    localStorage[localStorageKey] = $(this).attr("href");
                }
			});
			
			deferred.resolve();
		}).fail(function(response) {
            deferred.reject();
		});
        
        return deferred.promise();
    },
    	
	loadUsers: function(accessToken) {
		var deferred = $.Deferred();

		$.ajax({
			type: "GET",
			url: tables["Users"],
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0"
			}
		}).done(function(data) {
			var ballotDetails = [];

			$(data).find("entry").each(function() {				
				var firstName = $(this).find("firstname").text();
				var lastName = $(this).find("lastname").text();
				var userId = $(this).find("userid").text();
				var thiPollSites = $(this).find("pollsites").text();
				 if (localStorage.user_email == $(this).find("userid").text()) {
                          localStorage.pollSites = thiPollSites; 
				//	 console.log("THIS USER HAS RIGHT TO REPORT FORM SITES:" +  localStorage.pollSites )
					 }
			});		
		});
	}, 
	
		loadForms: function(accessToken) {
		var deferred = $.Deferred();
         // the url here is the google script web app url. it returns the json with description of forms
		$.ajax({
			type: "GET",
			url: "https://script.google.com/macros/s/AKfycbzSPFJuH6NoEbZvJ3MNVf74AYlIeZGtWFRXXRCZNvhVLZuvjGI/exec?initkey="+localStorage.activeInitiativeKey,
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0"
			}
		}).done(function(data) {
			var form = [];
		//	console.log('i am about to parse json for forms')
            var jsondata = JSON.parse(data);
		//	console.log('Yipiiiiiaeeeee I have parsed JSON!!!')
		 	var formPromise = localDataSaveForms(jsondata);
			$.when(formPromise).then(deferred.resolve, deferred.reject);
		}).fail(function(response) {
			deferred.reject();  
		});
		
	 	return deferred.promise(); 
	}, 
	
	
	loadVotingSites: function(accessToken) {
		var deferred = $.Deferred();
		$.ajax({
			type: "GET",
			url: tables["Places"],
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0"
			}
		}).done(function(data) {
			var votingSites = [];
			
			$(data).find("entry").each(function() {				
				var site = { };
				
                site.id = $(this).find("places").text();
				site.name = $(this).find("name").text();
				site.address = $(this).find("address").text();
				site.longitude = $(this).find("longitude").text();
				site.latitude = $(this).find("latitude").text();
			 	site.zipcode = $(this).find("zipcode").text();
				site.region = $(this).find("region").text();
				site.county = $(this).find("county").text();
				site.registeredvoters = $(this).find("registeredvoters").text();
				
				votingSites.push(site);
			});
			
            var sitePromise = localDataSaveVotingSites(votingSites);
			$.when(sitePromise).then(deferred.resolve, deferred.reject);
		}).fail(function(response) {
			deferred.reject();
		});
		return deferred.promise();	
	},
	
	
	loadTasks: function(accessToken) {
		var deferred = $.Deferred();

		$.ajax({
			type: "GET",
			url: tables["Tasks"],
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0"
			}
		}).done(function(data) {
			var tasks = [];
			
			$(data).find("entry").each(function() {				
				var task = { };
                task.task = $(this).find("task").text();
				task.menuentry = $(this).find("menuentry").text();
				task.type = $(this).find("type").text();
				task.data = $(this).find("data").text();
				task.sms = $(this).find("sms").text();
				tasks.push(task);
			});
			
            var taskPromise = localDataSaveTasks(tasks);
			$.when(taskPromise).then(deferred.resolve, deferred.reject);
		}).fail(function(response) {
			deferred.reject();
		});

		return deferred.promise();	
	},
	
	loadAppTexts: function(accessToken) {
		var deferred = $.Deferred();
		
		$.ajax({
			type: "GET",
			url: tables["App Text"],
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0"
			}
		}).done(function(data) {
			var appTexts = [];
			
			$(data).find("entry").each(function() {				
				var txt = { };
				
				txt.key = $(this).find("key").text();
				txt.value = $(this).find("value").text();
				
				appTexts.push(txt);
			});

            var appTextsPromise = localDataSaveAppTexts(appTexts);
			$.when(appTextsPromise).then(deferred.resolve, deferred.reject);
		}).fail(function(response) {
			deferred.reject();
		});
		
		return deferred.promise();
	}, 
 // the url here is the google script web app url. it saves the answers in the appropriate form
    saveResults: function(accessToken, url, bdy) {
		localStorage.sendSrriptURL = "https://script.google.com/macros/s/AKfycbyZQhahpD3CdqF-ryFgIkQBQ9lMRohVisSpfPGYqXDMuQlkA_Wg/exec"
        var d = new Date();     
		var deferred = $.Deferred();
		var postdata = '';
		for(var i=0; i<bdy.length; i++) {
		$.each(bdy[i], function(j, v) {
			postdata += j + '=' + v +'&'
			})
			}
			
			postdata += 'formkey='+ localStorage.noon_participation
      localStorage.lastPostData = postdata
      
        $.ajax({
			type: "GET", //il faut aussi essayer si post passe
			url:  "https://script.google.com/macros/s/AKfycbyZQhahpD3CdqF-ryFgIkQBQ9lMRohVisSpfPGYqXDMuQlkA_Wg/exec",
			headers: {
				'Authorization': "Bearer " + accessToken,
                'Content-type': "application/atom+xml"
			}, 
            data:postdata
		}).done(function(data) {
			console.log('save success: ' +postdata + 'response: ' + data)
            deferred.resolve();
		}).fail(function(response) {
			console.log('save failed: ' +postdata)
			deferred.reject();
		});
		
		return deferred.promise();   
    },
    
      saveUnsentResults: function(accessToken, bdy) {
		localStorage.sendSrriptURL = "https://script.google.com/macros/s/AKfycbyZQhahpD3CdqF-ryFgIkQBQ9lMRohVisSpfPGYqXDMuQlkA_Wg/exec"
          
		var deferred = $.Deferred();

        $.ajax({
			type: "GET", //il faut aussi essayer si post passe
			url:  "https://script.google.com/macros/s/AKfycbyZQhahpD3CdqF-ryFgIkQBQ9lMRohVisSpfPGYqXDMuQlkA_Wg/exec",
			headers: {
				'Authorization': "Bearer " + accessToken,
                'Content-type': "application/atom+xml"
			}, 
            data:bdy
		}).done(function(data) {
			console.log('save unsent success: ' +bdy + 'response: ' + data)
            deferred.resolve();
		}).fail(function(response) {
			console.log('save unsent failed: ' +bdy)
			deferred.reject();
		});
		
		return deferred.promise();   
    },
    
    checkConnection:   function() {
    var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown';
    states[Connection.ETHERNET] = 'Ethernet';
    states[Connection.WIFI]     = 'WiFi';
    states[Connection.CELL_2G]  = 'Cell';
    states[Connection.CELL_3G]  = 'Cell';
    states[Connection.CELL_4G]  = 'Cell';
    states[Connection.CELL]     = 'Cell';
    states[Connection.NONE]     = 'No';

    console.log('Connection type: ' + states[networkState]);
    localStorage.networkState = states[networkState];
} 
	

    
}
