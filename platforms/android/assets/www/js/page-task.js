/**************************************************************
*This file is part of Free & Fair, an electoral transparency mobile app.
*  Copyright (C) 2014 The Free & Fair Team
* 
*  Free & Fair is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*  
*  Free & Fair is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*  
*  You should have received a copy of the GNU General Public License
*  along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
****************************************************************/
$(document).on("pageinit", "#page-task", function(event) { 	
   
     function initData() {
				
        var items = "";
        
        $.when(localDataLoadTasks()).then(function (tasks) {

            $.each(tasks, function (index, task) {	 	 				
        items += '<li data-name="' + task.menuentry + '" data-type="'+task.type+'"'  + ' data-code="'+task.data+'"><a href="#"> ' + task.menuentry + '</a></li>'; 
            });
            
            $("#task-list").html(items);
            $("#task-list").listview("refresh");  
			$('#task-list').children('li').bind('mouseup', function(e) {
		
		localStorage.activeFormName =    $(this).attr('data-name');
	
		if( $(this).attr('data-type') == "FORM")	{ 
	//	 console.log('changing page to '+$(this).attr('data-type'));
		 $.mobile.changePage("#page-form", { transition: "none" });
		 $("#page-form").trigger('refresh')
	//	  console.log('should have changed page to '+$(this).attr('data-type'));
		  }
		else { 
	//	  console.log('changing page to '+$(this).attr('data-type'));
		  localStorage.activeFormName =    $(this).attr('data-name');
		  localStorage.activeFormCode =    $(this).attr('data-code');
		  $.mobile.changePage("#page-photo", { transition: "none" }); }
	});           
        });	
    }

    initData(); //first run
   
   
		 
	
    $('#submit-unsent-from-task').bind('mouseup', function(e) {
        $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
        
        var promise = localDataLoadUnsentResults();
        
        $.when(promise).then(function(unsent) {
            console.log('unsent step 2: ' + JSON.stringify(unsent[0]))
            var sendPromise = unsentDataHandler.send(unsent);
            $.when(sendPromise).then(dataSent, dataSendFailed);
        });
		
    });
	
	
	 $('#tasks-refresh').bind('mouseup', function (e) {
        $("#task-list").html("");
  $.mobile.loading("show", {
            textVisible: false,
            textonly: false,
            html: ''
        });
    
        googleapi.getToken(config.authOptions).done(function (data) {
            remoteData.loadData(data.access_token, function () {
                var promise = localDataLoadAppTexts();
                $.when(promise).then(function (data) {
                    localization = data;
                    localizeApp();  
                    initData(); 
                   dataLoaded();     
                });
            }, dataLoadFailed);
        }).fail(function (data) {
            dataLoadFailed();
        });
    });
    
	   function dataLoaded() {
        localStorage.data_loaded = "loaded";
        $.mobile.loading("hide");
        initData();
    }
    
    function dataLoadFailed() {
        $.mobile.loading("hide");
        $("#site-data-load-error").popup("open", { transition: "none", position: "window" });
        initData();
    }
	
	
    function dataSent() {
        $.mobile.loading("hide");
        $("#div-submit-unsent-from-task").hide();            
    }
    
    function dataSendFailed() {
        $.mobile.loading("hide");
        $("#task-data-send-error").popup("open", { transition: "none", position: "window" });        
    }
});

$(document).on("pageshow", "#page-task", function(event) { 
	  remoteData.checkConnection();
	 if(localStorage.networkState == 'No') {
    	
    	$("#div-refresh-ptasks").hide();
    	}    
    	else { $("#div-refresh-ptasks").show(); }
        
        
    $("#page-task-title").text(reportData.votingSite.id + ' ' + reportData.votingSite.name);

    var promise = localDataLoadUnsentResults();
    
    $.when(promise).then(function(unsentResults) {
        if (unsentResults.length > 0) {
            $("#div-submit-unsent-from-task").show();            
            unsentTask = unsentResults;
        } else {
            $("#div-submit-unsent-from-task").hide();            
        }
    });
    
    navigator.geolocation.getCurrentPosition(function(position) {
        reportData.longitude = position.coords.longitude;
        reportData.latitude = position.coords.latitude;
    }); 
});


