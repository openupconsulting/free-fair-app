var ongoingInitData = {
	tables: { },
	loadData: function(accessToken, successCallback, errorCallback) {
        ongoingInitData.loadEmail(accessToken, function() {		
	      var initiativeUri = 'https://spreadsheets.google.com/feeds/list/tEevBvSWe4_U081djVkL0Gw/od7/private/full'
           ongoingInitData.loadInitiatives(accessToken,initiativeUri, 
		   function() {
			   //    console.log('initdata.loaddata accessToken: ' + accessToken )
                    var initiativesPromise = ongoingInitData.loadInitiative(accessToken);   
                    $.when(initiativesPromise)
                    .then(successCallback, errorCallback);
                }
		   , errorCallback)
        }, errorCallback);
	},
	
    loadEmail: function(accessToken, callback, errorCallback) {
	//	console.log('loading email' )
		$.ajax({
			type: "GET",
			url: "https://www.googleapis.com/userinfo/email?alt=json",
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0" 
			}
		}).done(function(data) {
		//	 console.log('success email');
            localStorage.user_email = data.data.email;
		//	console.log(localStorage.user_email )
            callback();
		}).fail(function(response) {
		//	 console.log('fail load email');
            errorCallback();
		});		
    },

	loadInitiatives: function(accessToken, initiativeUri, callback, errorCallback) {
		
		$.ajax({
			type: "GET",
			url: initiativeUri,
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0"
			}
		}).done(function(data) {
			tables = { };
           
            var initPromise;
            
				//	console.log(initiativeUri);
                    initPromise = ongoingInitData.loadPostUri(accessToken, initiativeUri, "ONGOING");                    
        

				tables['ONGOING'] = initiativeUri;
	
			
            $.when(initPromise).then(function() {
                callback(tables);
            });
		}).fail(function(response) {
            errorCallback();
        });
	}, 
	
    loadPostUri: function(accessToken, uri, localStorageKey) {
        var deferred = $.Deferred();
     //   console.log('entering loadposturi')
        $.ajax({
			type: "GET",
			url: uri,
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0"
			}
		}).done(function(data) {
			$(data).find("link").each(function() {	 
                if ($(this).attr("rel") === "http://schemas.google.com/g/2005#post") {
                    localStorage[localStorageKey] = $(this).attr("href");
                }
			});
			
			deferred.resolve();
		}).fail(function(response) {
            deferred.reject();
		});
        
        return deferred.promise();
    },
    
	
	loadInitiative: function(accessToken) {
		var deferred = $.Deferred();
     
		$.ajax({
			type: "GET",
			url: tables['ONGOING'],
			headers: {
				'Authorization': "Bearer " + accessToken,
				'GData-Version': "3.0"
			}
		}).done(function(data) {
			var initiatives = [];
			
			$(data).find("entry").each(function() {				
				var initiative = { };
				
                initiative.country = $(this).find("country").text();
				initiative.initiative = $(this).find("initiative").text();
				initiative.menu = $(this).find("menuentry").text();
				initiative.date = $(this).find("date").text();
				initiative.expiration = $(this).find("expiration").text();
				initiative.key = $(this).find("key").text();
		 //       console.log('parsed initiative from xml' + $(this).find("key"))
				initiatives.push(initiative);
				
			});
			
            var initPromise = localDataSaveInitiatives(initiatives);
			$.when(initPromise).then(function() { 
			
			localStorage.initiatives_loaded = "loaded";
		//	console.log("initiatives loaded locally success!!! " + localStorage.initiatives_loaded)
			deferred.resolve();
			
			}, deferred.reject);  //  
		}).fail(function(response) {	deferred.reject(); });

		return deferred.promise();	
	} 
}


